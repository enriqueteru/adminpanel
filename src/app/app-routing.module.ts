import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PagesRoutingModule } from './pages/routes/pages.routing';
import { AuthRoutingModule } from './auth/routes/auth.routing';

import { Error404Component } from './error404/error404.component';



const routes: Routes = [
  // path: 'dashboard' PagesRouting
  // path: 'Auth' PagesAuth
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: '**', component: Error404Component },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    PagesRoutingModule,
    AuthRoutingModule,
  ],
  exports: [RouterModule, PagesRoutingModule, AuthRoutingModule],
})
export class AppRoutingModule {}
