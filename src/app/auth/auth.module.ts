import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import{SharedModule}from '../shared/shared.module'


import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';



@NgModule({
  declarations: [  LoginComponent,
    RegisterComponent],

  imports: [
    CommonModule,
    SharedModule,



  ],
  exports: [
    CommonModule,
    SharedModule

  ]
})
export class AuthModule { }
