import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  public urlTheme : string = "urlTheme"
  constructor() { }

  ngOnInit(): void {
  }

  changeTheme(theme: string) {
    const currentLinkTheme = document.querySelector('#theme');
    this.urlTheme = `./assets/css/colors/${theme}.css`
    currentLinkTheme?.setAttribute('href', this.urlTheme)
    console.log(currentLinkTheme)
    localStorage.setItem('theme', this.urlTheme)



  }

}
