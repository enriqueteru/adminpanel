import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: [
  ]
})
export class PagesComponent implements OnInit {
public linkItem = document.querySelector('#theme');

  constructor() { }

  ngOnInit(): void {

    const url = localStorage.getItem('theme') || './assets/css/colors/green-dark.css';
    this.linkItem?.setAttribute('href', url)
  }

}
