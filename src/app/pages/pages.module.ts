import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {SharedModule} from '../shared/shared.module'


import { DashboardComponent } from './dashboard/dashboard.component';
import {RouterModule } from '@angular/router'
import { AppRoutingModule } from '../app-routing.module';
import { PagesRoutingModule } from './routes/pages.routing';


import { ProgressComponent } from './progress/progress.component';
import { GraphicComponent } from './graphic/graphic.component';
import { PagesComponent } from './pages.component';
import { ComponentsModule } from '../components/components.module';
import { AccountSettingsComponent } from './account-settings/account-settings.component';



@NgModule({
  declarations: [
    ProgressComponent,
    GraphicComponent,
    PagesComponent,
    DashboardComponent,
    AccountSettingsComponent,

  ],

  exports: [
    ProgressComponent,
    GraphicComponent,
    PagesComponent,
    DashboardComponent,
    AccountSettingsComponent,


  ],


  imports: [CommonModule,FormsModule,
    SharedModule, RouterModule, AppRoutingModule, PagesRoutingModule, ComponentsModule],
})
export class PagesModule {}
