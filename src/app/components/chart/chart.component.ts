import { Component, Input } from '@angular/core';
import { ChartData, ChartType } from 'chart.js';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent {


@Input() getChartTitle?: string;

// PolarArea
public polarAreaChartLabels: string[] = [
  'Download Sales',
  'In-Store Sales',
  'Mail Sales',
  'Telesales',
  'Corporate Sales',
];
public polarAreaChartData: ChartData<'polarArea'> = {
  labels: this.polarAreaChartLabels,
  datasets: [
    {
      data: [300, 500, 100, 40, 120],
      label: 'Series 1',
    },
  ],
};
public polarAreaLegend = true;

public polarAreaChartType: ChartType = 'polarArea';

// events
public chartClicked({
  event,
  active,
}: {
  event: MouseEvent;
  active: {}[];
}): void {
  console.log(event, active);
}

public chartHovered({
  event,
  active,
}: {
  event: MouseEvent;
  active: {}[];
}): void {
  console.log(event, active);
}
}

