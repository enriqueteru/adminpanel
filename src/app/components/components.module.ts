import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { NgChartsModule } from 'ng2-charts';

import { IncrementadorComponent } from './incrementador/incrementador.component';
import { ChartComponent } from './chart/chart.component'

@NgModule({
  declarations: [IncrementadorComponent, ChartComponent],
  imports: [CommonModule, FormsModule, NgChartsModule],
  exports: [IncrementadorComponent, NgChartsModule,ChartComponent],
})
export class ComponentsModule {}
