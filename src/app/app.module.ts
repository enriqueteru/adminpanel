import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { PagesModule} from './pages/pages.module';
import { SharedModule} from './shared/shared.module';
import {AuthModule} from './auth/auth.module'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Error404Component } from './error404/error404.component';
import { ComponentsModule } from './components/components.module';



@NgModule({
  declarations: [
    AppComponent,
    Error404Component,




  ],
  imports: [
    BrowserModule,
    PagesModule,
    SharedModule,
    AuthModule,
    ComponentsModule,
    AppRoutingModule,




  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
